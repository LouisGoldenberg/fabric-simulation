#include "curtains_hacked.hpp"

#ifdef SCENE_CURTAINS_HACKED

using namespace vcl;

/** Compute spring force applied on particle pi from particle pj */
vec3 spring_force(const vec3& pi, const vec3& pj, float L0, float K)
{
    vec3 const pji = pj - pi;
    float const L = norm(pji);
    return K * (L - L0) * pji / L;
}

// Fill value of force applied on each particle
// - Gravity
// - Drag
// - Spring force
void scene_model::compute_forces()
{
    const size_t N = forceA.size();        // Total number of particles of the cloth Nu x Nv
    const size_t N_dim = forceA.dimension[0]; // Number of particles along one dimension (square dimension)
    // Same for B

    simulation_parameters.m = user_parameters.m / float(N); // Constant total mass

    // Get simuation parameters
    const float K = user_parameters.K;
    const float m = simulation_parameters.m;
    const float L0 = simulation_parameters.L0;

    const float K_shear = K;
    const float K_bend = K;

    // Gravity
    const vec3 g = { 0,-9.81f,0 };
    for (size_t k = 0; k < N; ++k)
    {
        forceA[k] = m * g;
        forceB[k] = m * g;
    }

    // Drag
    const float mu = user_parameters.mu;
    for (size_t k = 0; k < N; ++k)
    {
        forceA[k] = forceA[k] - mu * speedA[k];
        forceB[k] = forceB[k] - mu * speedB[k];
    }


    // Springs
    for (size_t ku = 0; ku < N_dim; ku++) {
        for (size_t kv = 0; kv < N_dim; kv++) {
            /* STRUCTURAL SPRINGS */
            if (ku > 0)
            {
                // vertical spring above
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku - 1) * N_dim + kv], L0, K);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku - 1) * N_dim + kv], L0, K);
            }
            if (ku + 1 < N_dim)
            {
                // vertical spring under
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku + 1) * N_dim + kv], L0, K);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku + 1) * N_dim + kv], L0, K);
            }
            if (kv > 0)
            {
                // horizontal spring left
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[ku * N_dim + kv - 1], L0, K);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[ku * N_dim + kv - 1], L0, K);
            }
            if (kv + 1 < N_dim)
            {
                // horizontal spring right
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[ku * N_dim + kv + 1], L0, K);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[ku * N_dim + kv + 1], L0, K);
            }
            /* SHEARING SPRINGS */
            if (ku > 0 && kv > 0)
            {
                // diagonal upper left
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku - 1) * N_dim + kv - 1], std::sqrt(2.0f) * L0, K_shear);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku - 1) * N_dim + kv - 1], std::sqrt(2.0f) * L0, K_shear);
            }
            if (ku > 0 && kv + 1 < N_dim)
            {
                // diagonal upper right
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku - 1) * N_dim + kv + 1], std::sqrt(2.0f) * L0, K_shear);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku - 1) * N_dim + kv + 1], std::sqrt(2.0f) * L0, K_shear);
            }
            if (ku + 1 < N_dim && kv > 0)
            {
                // diagonal lower left
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku + 1) * N_dim + kv - 1], std::sqrt(2.0f) * L0, K_shear);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku + 1) * N_dim + kv - 1], std::sqrt(2.0f) * L0, K_shear);
            }
            if (ku + 1 < N_dim && kv + 1 < N_dim)
            {
                // diagonal lower right
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku + 1) * N_dim + kv + 1], std::sqrt(2.0f) * L0, K_shear);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku + 1) * N_dim + kv + 1], std::sqrt(2.0f) * L0, K_shear);
            }
            /* BENDING SPRINGS */
            if (ku > 1)
            {
                // twice above
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku - 2) * N_dim + kv], 2.0f * L0, K_bend);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku - 2) * N_dim + kv], 2.0f * L0, K_bend);
            }
            if (kv > 1)
            {
                // twice left
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[ku * N_dim + kv - 2], 2.0f * L0, K_bend);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[ku * N_dim + kv - 2], 2.0f * L0, K_bend);
            }
            if (ku + 2 < N_dim)
            {
                // twice below
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[(ku + 2) * N_dim + kv], 2.0f * L0, K_bend);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[(ku + 2) * N_dim + kv], 2.0f * L0, K_bend);
            }
            if (kv + 2 < N_dim)
            {
                // twice right
                forceA[ku * N_dim + kv] += spring_force(positionA[ku * N_dim + kv], positionA[ku * N_dim + kv + 2], 2.0f * L0, K_bend);
                forceB[ku * N_dim + kv] += spring_force(positionB[ku * N_dim + kv], positionB[ku * N_dim + kv + 2], 2.0f * L0, K_bend);
            }

        }
    }

}

// Handle detection and response to collision with the shape described in "collision_shapes" variable
void scene_model::collision_constraints()
{
    // Handle collisions here (with the ground and the sphere)
    const size_t N = forceA.size();
    float const collision_margin = 0.0f;

    /* COLLISION WITH THE GROUND */
    for (size_t k = 0; k < N; k++)
    {
        float& z = positionA[k][1];
        float& v = speedA[k][1];
        if (z < collision_shapes.ground_height + collision_margin)
        {
            z = collision_shapes.ground_height + collision_margin;
            v = 0;
        }
    }
    /* COLLISION WITH THE SEPARATING PLANE */
    for (size_t k = 0; k < N; k++)
    {
        float& x = positionA[k][0];
        float& v = speedA[k][0];
        if (x < collision_shapes.sep_plane_coord + collision_margin)
        {
            x = collision_shapes.sep_plane_coord + collision_margin;
            v = 0;
        }
    }

    // Same with cloth B
    // 
    // Handle collisions here (with the ground and the sphere)

    /* COLLISION WITH THE GROUND */
    for (size_t k = 0; k < N; k++)
    {
        float& z = positionB[k][1];
        float& v = speedB[k][1];
        if (z < collision_shapes.ground_height + collision_margin)
        {
            z = collision_shapes.ground_height + collision_margin;
            v = 0;
        }
    }
    /* COLLISION WITH THE SEPARATING PLANE */
    for (size_t k = 0; k < N; k++)
    {
        float& x = positionB[k][0];
        float& v = speedB[k][0];
        if (x > - collision_shapes.sep_plane_coord - collision_margin)
        {
            x = - collision_shapes.sep_plane_coord - collision_margin;
            v = 0;
        }
    }

}

// Initialize the geometrical model
void scene_model::initialize()
{
    // Number of samples of the model (total number of particles is N_cloth x N_cloth)
    // Ideally set to 10*k + 1 for any integer k
    const size_t N_cloth = 31;

    // Rest length (length of an edge)
    simulation_parameters.L0 = 1.0f / float(N_cloth - 1);

    // Create cloth mesh in its initial position
    // Vertical grid of length 1 x 1
    const mesh base_clothA = mesh_primitive_grid(N_cloth, N_cloth, { 0,1.2f,0 }, { 0,-1,0.1f }, { 1.0f,0,0 });
    const mesh base_clothB = mesh_primitive_grid(N_cloth, N_cloth, { 0,1.2f,0 }, { 0,-1,0.1f }, { -1.0f,0,0 });

    // Set particle position from cloth geometry
    positionA = buffer2D_from_vector(base_clothA.position, N_cloth, N_cloth);
    positionB = buffer2D_from_vector(base_clothB.position, N_cloth, N_cloth);

    // Set hard positional constraints
    simulation_parameters.opening_curtains_A = true;
    simulation_parameters.opening_curtains_B = true;
    for (int i = 0; i < N_cloth; i += 10)
    {
        positional_constraintsA[i * N_cloth] = positionA[i * N_cloth];
        positional_constraintsB[i * N_cloth] = positionB[i * N_cloth];
    }

    // Init particles data (speed, force)
    speedA.resize(positionA.dimension); speedA.fill({ 0,0,0 });
    forceA.resize(positionA.dimension); forceA.fill({ 0,0,0 });

    speedB.resize(positionB.dimension); speedB.fill({ 0,0,0 });
    forceB.resize(positionB.dimension); forceB.fill({ 0,0,0 });


    // Store connectivity and normals
    connectivityA = base_clothA.connectivity;
    normalsA = normal(positionA.data, connectivityA);

    connectivityB = base_clothB.connectivity;
    normalsB = normal(positionB.data, connectivityB);

    // Send data to GPU
    clothA.clear();
    clothA = mesh_drawable(base_clothA);
    clothA.uniform.shading.specular = 0.0f;
    clothA.shader = shader_mesh;
    clothA.texture_id = texture_cloth;

    clothB.clear();
    clothB = mesh_drawable(base_clothB);
    clothB.uniform.shading.specular = 0.0f;
    clothB.shader = shader_mesh;
    clothB.texture_id = texture_cloth;

    simulation_diverged = false;
    force_simulation = false;

    timer.update();
}

void scene_model::setup_data(std::map<std::string, GLuint>& shaders, scene_structure&, gui_structure& gui)
{
    gui.show_frame_camera = false;

    // Load textures
    texture_cloth = create_texture_gpu(image_load_png("scenes/refreshers/02_simulation/assets/curtain.png"));
    texture_wood = create_texture_gpu(image_load_png("scenes/refreshers/02_simulation/assets/parquet.png"));
    shader_mesh = shaders["mesh_bf"];

    // Initialize cloth geometry and particles
    initialize();

    // Default value for simulation parameters
    user_parameters.K = 100.0f;
    user_parameters.m = 5.0f;
    user_parameters.mu = 0.02f;
    user_parameters.opening_speed = 0.0f;

    // Set collision shapes
    collision_shapes.ground_height = 0.1f;
    collision_shapes.sep_plane_coord = 0.0f;

    // Init visual models
    sphere = mesh_drawable(mesh_primitive_sphere(1.0f, { 0,0,0 }, 60, 60));
    sphere.shader = shaders["mesh"];
    sphere.uniform.color = { 1,0,0 };

    ground = mesh_drawable(mesh_primitive_quad({ -1,collision_shapes.ground_height - 1e-3f,-1 }, { 1,collision_shapes.ground_height - 1e-3f,-1 }, { 1,collision_shapes.ground_height - 1e-3f,1 }, { -1,collision_shapes.ground_height - 1e-3f,1 }));
    ground.shader = shaders["mesh_bf"];
    ground.texture_id = texture_wood;

    gui_display_texture = true;
    gui_display_wireframe = false;
}

void scene_model::frame_draw(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure& gui)
{
    const float dt = timer.update();
    set_gui();

    // Force constant simulation time step
    float h = dt <= 1e-6f ? 0.0f : timer.scale * 0.001f;

    if ((!simulation_diverged || force_simulation) && h > 0)
    {
        // Iterate over a fixed number of substeps between each frames
        const size_t number_of_substeps = 4;
        for (size_t k = 0; (!simulation_diverged || force_simulation) && k < number_of_substeps; ++k)
        {
            compute_forces();
            numerical_integration(h);
            collision_constraints();                            // Detect and solve collision with other shapes

            hard_constraints(h);                                // Enforce hard positional constraints

            normal(positionA.data, connectivityA, normalsA);    // Update normals of the cloth
            normal(positionB.data, connectivityB, normalsB);    // Update normals of the cloth
            detect_simulation_divergence();                     // Check if the simulation seems to diverge
        }
    }


    clothA.update_position(positionA.data);
    clothA.update_normal(normalsA.data);

    clothB.update_position(positionB.data);
    clothB.update_normal(normalsB.data);

    display_elements(shaders, scene, gui);

}

void scene_model::numerical_integration(float h)
{
    const size_t NN = positionA.size(); // same for B
    const float m = simulation_parameters.m;

    for (size_t k = 0; k < NN; ++k)
    {
        vec3& p = positionA[k];
        vec3& v = speedA[k];
        const vec3& f = forceA[k];

        v = v + h * f / m;
        p = p + h * v;
    }
    // same for B
    for (size_t k = 0; k < NN; ++k)
    {
        vec3& p = positionB[k];
        vec3& v = speedB[k];
        const vec3& f = forceB[k];

        v = v + h * f / m;
        p = p + h * v;
    }
}

void scene_model::hard_constraints(float h)
{
    if (simulation_parameters.opening_curtains_A)
    {
        // Moving hard constraints
        for (auto& constraints : positional_constraintsA)
        {
            vec3 speed_vec = { 0.01f * user_parameters.opening_speed, 0, 0 };
            constraints.second = constraints.second + h * speed_vec * (positionA.size() - constraints.first) / 10;
            positionA[constraints.first] = constraints.second;
        }
        // Automatic stop
        auto constraint_leftA = std::cbegin(positional_constraintsA);     // first constraint
        auto constraint_rightA = std::crbegin(positional_constraintsA);   // last constraint
        if (norm(constraint_leftA->second - constraint_rightA->second) <= 0.1f || norm(constraint_leftA->second - constraint_rightA->second) > 1.0f)
        {
            simulation_parameters.opening_curtains_A = false;
        }
    }
    else
    {
        // Fixed positions of the cloth
        for (const auto& constraints : positional_constraintsA)
        {
            positionA[constraints.first] = constraints.second;
        }
    }


    // Same for B

    if (simulation_parameters.opening_curtains_B)
    {
        // Moving hard constraints
        for (auto& constraints : positional_constraintsB)
        {
            vec3 speed_vec = { -0.01f * user_parameters.opening_speed, 0, 0 };
            constraints.second = constraints.second + h * speed_vec * (positionB.size() - constraints.first) / 10;
            positionB[constraints.first] = constraints.second;
        }
        // Automatic stop
        auto constraint_leftB = std::cbegin(positional_constraintsB);     // first constraint
        auto constraint_rightB = std::crbegin(positional_constraintsB);   // last constraint
        if (norm(constraint_leftB->second - constraint_rightB->second) <= 0.1f || norm(constraint_leftB->second - constraint_rightB->second) > 1.0f)
        {
            simulation_parameters.opening_curtains_B = false;
        }
    }
    else
    {
        // Fixed positions of the cloth
        for (const auto& constraints : positional_constraintsB)
        {
            positionB[constraints.first] = constraints.second;
        }
    }

}

void scene_model::display_elements(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure&)
{
    glEnable(GL_POLYGON_OFFSET_FILL);

    // Display cloth A
    {
        GLuint texture = clothA.texture_id;
        if (!gui_display_texture)
            texture = scene.texture_white;

        glPolygonOffset(1.0, 1.0);
        draw(clothA, scene.camera, clothA.shader, texture);
        glBindTexture(GL_TEXTURE_2D, scene.texture_white);

        if (gui_display_wireframe) {
            glPolygonOffset(1.0, 1.0);
            draw(clothA, scene.camera, shaders["wireframe_quads"]);
        }
    }

    {
        // Display cloth B
        GLuint texture = clothA.texture_id;
        if (!gui_display_texture)
            texture = scene.texture_white;

        glPolygonOffset(1.0, 1.0);
        draw(clothB, scene.camera, clothB.shader, texture);
        glBindTexture(GL_TEXTURE_2D, scene.texture_white);

        if (gui_display_wireframe) {
            glPolygonOffset(1.0, 1.0);
            draw(clothB, scene.camera, shaders["wireframe_quads"]);
        }
    }

    // Display positional constraint using spheres
    sphere.uniform.transform.scaling = 0.02f;
    for (const auto& constraints : positional_constraintsA) {
        sphere.uniform.transform.translation = constraints.second;
        draw(sphere, scene.camera, shaders["mesh"]);
    }

    // Same for B
    sphere.uniform.transform.scaling = 0.02f;
    for (const auto& constraints : positional_constraintsB) {
        sphere.uniform.transform.translation = constraints.second;
        draw(sphere, scene.camera, shaders["mesh"]);
    }

    // Display ground
    draw(ground, scene.camera);
    glBindTexture(GL_TEXTURE_2D, scene.texture_white);
}

// Automatic detection of divergence: stop the simulation if detected
void scene_model::detect_simulation_divergence()
{
    const size_t NN = positionA.size(); // same for B

    for (size_t k = 0; simulation_diverged == false && k < NN; ++k)
    {
        const float fA = norm(forceA[k]);
        const float fB = norm(forceB[k]);
        const vec3& pA = positionA[k];
        const vec3& pB = positionB[k];

        if (std::isnan(fA) || std::isnan(fB)) // detect NaN in force
        {
            std::cout << "NaN detected in forces" << std::endl;
            simulation_diverged = true;
        }

        if (fA > 1000.0f || fB > 1000.0f) // detect strong force magnitude
        {
            std::cout << " **** Warning : Strong force magnitude detected " << " ****" << std::endl;
            simulation_diverged = true;
        }

        if (std::isnan(pA.x) || std::isnan(pA.y) || std::isnan(pA.z) || std::isnan(pB.x) || std::isnan(pB.y) || std::isnan(pB.z)) // detect NaN in position
        {
            std::cout << "NaN detected in positions" << std::endl;
            simulation_diverged = true;
        }

        if (simulation_diverged == true)
        {
            std::cerr << " **** Simulation has diverged **** " << std::endl;
            std::cerr << " > Stop simulation iterations" << std::endl;
            timer.stop();
        }
    }

}

void scene_model::set_gui()
{
    ImGui::SliderFloat("Time scale", &timer.scale, 0.05f, 2.0f, "%.2f s");
    ImGui::SliderFloat("Stiffness", &user_parameters.K, 1.0f, 400.0f, "%.2f s");
    ImGui::SliderFloat("Damping", &user_parameters.mu, 0.0f, 0.1f, "%.3f s");
    ImGui::SliderFloat("Mass", &user_parameters.m, 1.0f, 15.0f, "%.2f s");
    ImGui::SliderFloat("Opening_speed", &user_parameters.opening_speed, -5.0f, 5.0f, "%0.1f s");

    ImGui::Checkbox("Wireframe", &gui_display_wireframe);
    ImGui::Checkbox("Texture", &gui_display_texture);

    bool const stop = ImGui::Button("Stop anim"); ImGui::SameLine();
    bool const start = ImGui::Button("Start anim");

    if (stop)  timer.stop();
    if (start) {
        if (simulation_diverged)
            force_simulation = true;
        timer.start();
    }

    bool const restart = ImGui::Button("Restart");
    if (restart) initialize();

    bool const open_curtains = ImGui::Button("Open curtains");
    if (open_curtains)
    {
        simulation_parameters.opening_curtains_A = true;
        simulation_parameters.opening_curtains_B = true;
    }
}


#endif