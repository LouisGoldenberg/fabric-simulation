#define _USE_MATH_DEFINES

#include <cmath>
#include "character.hpp"


#ifdef SCENE_CHARACTER

using namespace vcl;

/** Compute spring force applied on particle pi from particle pj */
vec3 spring_force(const vec3& pi, const vec3& pj, float L0, float K)
{
    vec3 const pji = pj - pi;
    float const L = norm(pji);
    return K * (L - L0) * pji / L;
}

// Fill value of force applied on each particle
// - Gravity
// - Drag
// - Spring force
// - Wind force
void scene_model::compute_forces()
{
    const size_t N = force.size();        // Total number of particles of the cloth Nu x Nv
    const size_t N_dim = force.dimension[0]; // Number of particles along one dimension (square dimension)

    simulation_parameters.m = user_parameters.m / float(N); // Constant total mass

    // Get simuation parameters
    const float K = user_parameters.K;
    const float m = simulation_parameters.m;
    const float L0 = simulation_parameters.L0;

    const float K_shear = K;
    const float K_bend = K;

    // Gravity
    const vec3 g = { 0,-9.81f,0 };
    for (size_t k = 0; k < N; ++k)
        force[k] = m * g;

    // Drag
    const float mu = user_parameters.mu;
    for (size_t k = 0; k < N; ++k)
        force[k] = force[k] - mu * speed[k];

    // Springs
    for (size_t ku = 0; ku < N_dim; ku++) {
        for (size_t kv = 0; kv < N_dim; kv++) {
            /* STRUCTURAL SPRINGS */
            if (ku > 0)
            {
                // vertical spring above
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku - 1) * N_dim + kv], L0, K);
            }
            if (ku + 1 < N_dim)
            {
                // vertical spring under
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku + 1) * N_dim + kv], L0, K);
            }
            if (kv > 0)
            {
                // horizontal spring left
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[ku * N_dim + kv - 1], L0, K);
            }
            if (kv + 1 < N_dim)
            {
                // horizontal spring right
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[ku * N_dim + kv + 1], L0, K);
            }
            /* SHEARING SPRINGS */
            if (ku > 0 && kv > 0)
            {
                // diagonal upper left
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku - 1) * N_dim + kv - 1], std::sqrt(2.0f) * L0, K_shear);
            }
            if (ku > 0 && kv + 1 < N_dim)
            {
                // diagonal upper right
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku - 1) * N_dim + kv + 1], std::sqrt(2.0f) * L0, K_shear);
            }
            if (ku + 1 < N_dim && kv > 0)
            {
                // diagonal lower left
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku + 1) * N_dim + kv - 1], std::sqrt(2.0f) * L0, K_shear);
            }
            if (ku + 1 < N_dim && kv + 1 < N_dim)
            {
                // diagonal lower right
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku + 1) * N_dim + kv + 1], std::sqrt(2.0f) * L0, K_shear);
            }
            /* BENDING SPRINGS */
            if (ku > 1)
            {
                // twice above
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku - 2) * N_dim + kv], 2 * L0, K_bend);
            }
            if (kv > 1)
            {
                // twice left
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[ku * N_dim + kv - 2], 2 * L0, K_bend);
            }
            if (ku + 2 < N_dim)
            {
                // twice below
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[(ku + 2) * N_dim + kv], 2 * L0, K_bend);
            }
            if (kv + 2 < N_dim)
            {
                // twice right
                force[ku * N_dim + kv] += spring_force(position[ku * N_dim + kv], position[ku * N_dim + kv + 2], 2 * L0, K_bend);
            }

        }
    }

    // Wind force
    vec3 wind_direction = { cos(user_parameters.wind_angle), 0.0f, sin(user_parameters.wind_angle) };
    vec3 wind = 0.01f * user_parameters.wind * wind_direction;

    for (size_t k = 0; k < N; k++)
    {
        // First solution : force along the wind_direction 
        //force[k] += dot(wind, normals[k]) * wind_direction;

        // Second solution : force along the normal to the surface
        force[k] += dot(wind, normals[k]) * normals[k];
    }

}

// if the point is in the cylinder 
bool in_cylinder(vec3 point, vec3 p1, vec3 p2, float radius, float margin)
{
    vec3 unit_1_to_2 = (p2 - p1) / norm(p2 - p1);
    vec3 axis_projection = dot(point - p1, unit_1_to_2) * unit_1_to_2 + p1;
    bool near_from_axis = norm(point - axis_projection) < radius + margin;
    bool side_1 = dot(point - p1, unit_1_to_2) >= -margin;
    bool side_2 = dot(point - p2, - unit_1_to_2) >= -margin;
    return near_from_axis && side_1 && side_2;
}

struct pos_speed {
    vec3 pos;
    vec3 vit;
};

pos_speed push_out_cylinder(vec3 point, vec3 vit, vec3 p1, vec3 p2, float radius, float margin)
{
    vec3 unit_1_to_2 = (p2 - p1) / norm(p2 - p1);
    vec3 axis_projection = dot(point - p1, unit_1_to_2) * unit_1_to_2 + p1;
    vec3 shifted_top = - margin * unit_1_to_2 + p1;
    vec3 shifted_bottom = margin * unit_1_to_2 + p2;

    float dist_to_top = norm(axis_projection - shifted_top);
    float dist_to_bottom = norm(axis_projection - shifted_bottom);
    float dist_to_side = radius + margin - norm(point - axis_projection);

    pos_speed result;
    if (dist_to_side <= dist_to_top && dist_to_side <= dist_to_bottom)
    {
        // push to side
        result.pos = axis_projection + (point - axis_projection) / norm(point - axis_projection) * (radius + margin);

    }
    else if (dist_to_top <= dist_to_side && dist_to_top <= dist_to_bottom)
    {
        // push to top
        result.pos = shifted_top + (point - axis_projection);
        result.vit = vit - dot(vit, unit_1_to_2) * unit_1_to_2;
    }
    else
    {
        // push to bottom
        result.pos = shifted_bottom + (point - axis_projection);
        result.vit = vit - dot(vit, unit_1_to_2) * unit_1_to_2;
    }
    return result;
}


// Handle detection and response to collision with the shape described in "collision_shapes" variable
void scene_model::collision_constraints()
{
    // Handle collisions here (with the ground and the sphere)
    const size_t N = force.size();
    float const collision_margin = 0.025f;

    /* COLLISION WITH THE GROUND */
    for (size_t k = 0; k < N; k++)
    {
        float& z = position[k][1];
        float& vit = speed[k][1];
        if (z < collision_shapes.ground_height + collision_margin)
        {
            z = collision_shapes.ground_height + collision_margin;
            vit = 0;
        }
    }

    /* COLLISION WITH THE BODY */

    for (size_t k = 0; k < N; k++)
    {
        // get position and speed
        vec3& pos = position[k];
        vec3& vit = speed[k];

        // compute collisions with every part of the body
        
        // Cylinders
        for (const auto& c : body_shape.cylinders)
        {
            if (in_cylinder(pos, c.p1, c.p2, c.radius, collision_margin))
            {
                pos_speed ps = push_out_cylinder(pos, vit, c.p1, c.p2, c.radius, collision_margin);
                pos = ps.pos;
                vit = ps.vit;
            }
        }
        // Spheres
        for (const auto& s : body_shape.spheres)
        {
            if (norm(pos - s.center) < s.radius + collision_margin)
            {
                vec3 normal_vector = (pos - s.center) / norm(pos - s.center);
                pos = s.center + normal_vector * (s.radius + collision_margin);
                vit = vit - dot(vit, normal_vector) * normal_vector;
            }
        }

    }
}



// Initialize the geometrical model
void scene_model::initialize()
{
    // Number of samples of the model (total number of particles is N_cloth x N_cloth)
    const size_t N_cloth = 30;

    // Rest length (length of an edge)
    simulation_parameters.L0 = 1.2f / float(N_cloth - 1);

    // Create cloth mesh in its initial position
    // Horizontal grid of length 1 x 1
    const mesh base_cloth = mesh_primitive_grid(N_cloth, N_cloth, { 0.1f,1.22f,-0.6f }, { 0,0,1.2f }, { -1.2f,0,0 });

    // Set particle position from cloth geometry
    position = buffer2D_from_vector(base_cloth.position, N_cloth, N_cloth);

    // Set hard positional constraints
    positional_constraints[0] = { 0.2f,1.15f,-0.15f };
    positional_constraints[N_cloth - 1] = { 0.2f,1.15f,0.15f };

    // Init particles data (speed, force)
    speed.resize(position.dimension); speed.fill({ 0,0,0 });
    force.resize(position.dimension); force.fill({ 0,0,0 });


    // Store connectivity and normals
    connectivity = base_cloth.connectivity;
    normals = normal(position.data, connectivity);

    // Send data to GPU
    cloth.clear();
    cloth = mesh_drawable(base_cloth);
    cloth.uniform.shading.specular = 0.0f;
    cloth.shader = shader_mesh;
    cloth.texture_id = texture_cloth;

    simulation_diverged = false;
    force_simulation = false;

    timer.update();
}

void scene_model::setup_data(std::map<std::string, GLuint>& shaders, scene_structure&, gui_structure& gui)
{
    gui.show_frame_camera = false;

    // Load textures
    texture_cloth = create_texture_gpu(image_load_png("scenes/refreshers/02_simulation/assets/superman.png"));
    texture_wood = create_texture_gpu(image_load_png("scenes/refreshers/02_simulation/assets/ground.png"));
    shader_mesh = shaders["mesh_bf"];

    // Initialize cloth geometry and particles
    initialize();

    // Default value for simulation parameters
    user_parameters.K = 100.0f;
    user_parameters.m = 5.0f;
    user_parameters.wind = 10.0f;
    user_parameters.wind_angle = M_PI;
    user_parameters.mu = 0.02f;

    // Set collision shapes
    collision_shapes.ground_height = -0.1f;
    
    // Set body parameters
     
    // head 
    body_shape.head_p = { 0,1.5f,0 };
    body_shape.head_r = 0.25f;

    // chest
    body_shape.chest_top = { 0,1.2f,0 };
    body_shape.chest_bottom = { 0,0.5f,0 };
    body_shape.chest_r = 0.25f;

    // arms
    body_shape.shoulder_left = { 0,1.1f,-0.25f };
    body_shape.shoulder_right = { 0,1.1f,0.25f };
    body_shape.elbow_left = { 0.45f,0.85f,-0.25f };
    body_shape.elbow_right = { 0.45f,0.9f,0.25f };
    body_shape.hand_left = { 0.35f,0.75f,0.15f };
    body_shape.hand_right = { 0.3f,0.8f,-0.15f };
    body_shape.shoulder_r = 0.1f;
    body_shape.arm_r = 0.08f;
    body_shape.hand_r = 0.1f;

    // legs
    body_shape.hip_left = { 0,0.5f,-0.15f };
    body_shape.knee_left = { 0,0.25f,-0.15f };
    body_shape.foot_left = { 0,0,-0.15f };
    body_shape.hip_right = { 0,0.5f,0.15f };
    body_shape.knee_right = { 0,0.25f,0.15f };
    body_shape.foot_right = { 0,0,0.15f };
    body_shape.leg_r = 0.1f;
    body_shape.foot_r = 0.12f;

    // Set body shapes
    sphere_structure s_head = { body_shape.head_p, body_shape.head_r };
    sphere_structure s_left_shoulder = { body_shape.shoulder_left, body_shape.shoulder_r };
    sphere_structure s_right_shoulder = { body_shape.shoulder_right, body_shape.shoulder_r };
    sphere_structure s_left_elbow = { body_shape.elbow_left, body_shape.arm_r };
    sphere_structure s_right_elbow = { body_shape.elbow_right, body_shape.arm_r };
    sphere_structure s_left_hand = { body_shape.hand_left, body_shape.hand_r };
    sphere_structure s_right_hand = { body_shape.hand_right, body_shape.hand_r };
    sphere_structure s_left_knee = { body_shape.knee_left, body_shape.leg_r };
    sphere_structure s_right_knee = { body_shape.knee_right, body_shape.leg_r };
    sphere_structure s_left_foot = { body_shape.foot_left, body_shape.foot_r };
    sphere_structure s_right_foot = { body_shape.foot_right, body_shape.foot_r };

    body_shape.spheres = { s_head, s_left_shoulder, s_right_shoulder, s_left_elbow, s_right_elbow, s_left_hand, s_right_hand, s_left_knee, s_right_knee, s_left_foot, s_right_foot };

    cylinder_structure c_chest = { body_shape.chest_top, body_shape.chest_bottom, body_shape.chest_r };
    cylinder_structure c_left_arm = { body_shape.shoulder_left, body_shape.elbow_left, body_shape.arm_r };
    cylinder_structure c_right_arm = { body_shape.shoulder_right, body_shape.elbow_right, body_shape.arm_r };
    cylinder_structure c_left_forearm = { body_shape.elbow_left, body_shape.hand_left, body_shape.arm_r };
    cylinder_structure c_right_forearm = { body_shape.elbow_right, body_shape.hand_right, body_shape.arm_r };
    cylinder_structure c_left_thigh = { body_shape.hip_left, body_shape.knee_left, body_shape.leg_r };
    cylinder_structure c_right_thigh = { body_shape.hip_right, body_shape.knee_right, body_shape.leg_r };
    cylinder_structure c_left_calf = { body_shape.knee_left, body_shape.foot_left, body_shape.leg_r };
    cylinder_structure c_right_calf = { body_shape.knee_right, body_shape.foot_right, body_shape.leg_r };

    body_shape.cylinders = { c_chest, c_left_arm, c_right_arm, c_left_forearm, c_right_forearm, c_left_thigh, c_right_thigh, c_left_calf, c_right_calf };

    disc_structure d_chest_top = { body_shape.chest_top, body_shape.chest_r, body_shape.chest_top - body_shape.chest_bottom };
    disc_structure d_chest_bottom = { body_shape.chest_bottom, body_shape.chest_r, body_shape.chest_bottom - body_shape.chest_top };

    body_shape.discs = { d_chest_top, d_chest_bottom };

    // Init visual models
    sphere = mesh_drawable(mesh_primitive_sphere(1.0f, { 0,0,0 }, 60, 60));
    sphere.shader = shaders["mesh"];
    sphere.uniform.color = { 1,0,0 };

    cylinder = mesh_drawable(mesh_primitive_cylinder(1.0f, { 0,0,0 }, { 0,1,0 }, 60, 60));
    cylinder.shader = shaders["mesh"];
    cylinder.uniform.color = { 0,0,1 };

    disc = mesh_drawable(mesh_primitive_disc(1.0f, { 0,0,0 }, { 0,1,0 }, 60));
    disc.shader = shaders["mesh"];
    disc.uniform.color = { 0,0,1 };

    ground = mesh_drawable(mesh_primitive_quad({ -1,collision_shapes.ground_height - 1e-3f,-1 }, { 1,collision_shapes.ground_height - 1e-3f,-1 }, { 1,collision_shapes.ground_height - 1e-3f,1 }, { -1,collision_shapes.ground_height - 1e-3f,1 }));
    ground.shader = shaders["mesh_bf"];
    ground.texture_id = texture_wood;

    gui_display_texture = true;
    gui_display_wireframe = false;
}

void scene_model::frame_draw(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure& gui)
{
    const float dt = timer.update();
    set_gui();

    // Force constant simulation time step
    float h = dt <= 1e-6f ? 0.0f : timer.scale * 0.001f;

    if ((!simulation_diverged || force_simulation) && h > 0)
    {
        // Iterate over a fixed number of substeps between each frames
        const size_t number_of_substeps = 4;
        for (size_t k = 0; (!simulation_diverged || force_simulation) && k < number_of_substeps; ++k)
        {
            compute_forces();
            numerical_integration(h);
            collision_constraints();                 // Detect and solve collision with other shapes

            hard_constraints();                      // Enforce hard positional constraints

            normal(position.data, connectivity, normals); // Update normals of the cloth
            detect_simulation_divergence();               // Check if the simulation seems to diverge
        }
    }


    cloth.update_position(position.data);
    cloth.update_normal(normals.data);

    display_elements(shaders, scene, gui);

}

void scene_model::numerical_integration(float h)
{
    const size_t NN = position.size();
    const float m = simulation_parameters.m;

    for (size_t k = 0; k < NN; ++k)
    {
        vec3& p = position[k];
        vec3& v = speed[k];
        const vec3& f = force[k];

        v = v + h * f / m;
        p = p + h * v;
    }
}

void scene_model::hard_constraints()
{
    // Fixed positions of the cloth
    for (const auto& constraints : positional_constraints)
        position[constraints.first] = constraints.second;
}


void scene_model::display_elements(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure&)
{
    glEnable(GL_POLYGON_OFFSET_FILL);

    // Display cloth
    GLuint texture = cloth.texture_id;
    if (!gui_display_texture)
        texture = scene.texture_white;

    glPolygonOffset(1.0, 1.0);
    draw(cloth, scene.camera, cloth.shader, texture);
    glBindTexture(GL_TEXTURE_2D, scene.texture_white);

    if (gui_display_wireframe) {
        glPolygonOffset(1.0, 1.0);
        draw(cloth, scene.camera, shaders["wireframe_quads"]);
    }

    // Display character shapes

    // spheres
    sphere.uniform.color = { 0,0,1 };
    for (const auto& s : body_shape.spheres)
    {
        sphere.uniform.transform.scaling = s.radius;
        sphere.uniform.transform.translation = s.center;
        draw(sphere, scene.camera);
    }

    // cylinders
    for (const auto& c : body_shape.cylinders)
    {
        // axis_scaling
        vec3 axis_scaling = { c.radius, norm(c.p2 - c.p1), c.radius };
        cylinder.uniform.transform.scaling_axis = axis_scaling;

        // Compute the rotation that maps (0,1,0) to (p2-p1)/norm(p2-p1)
        vec3 const unit_y = { 0,1,0 };
        vec3 const unit_final = (c.p2 - c.p1) / norm(c.p2 - c.p1);
        float const tolerance = 0.00001f; // 10.e^(-5)
        if (fabs(dot(unit_y, unit_final) + 1) < tolerance)
        {
            // if the two unit vectors are opposite one to the other
            cylinder.uniform.transform.rotation = -mat3::identity();
        }
        else
        {
            // use cross product and dot product
            vec3 cross_prod = cross(unit_y, unit_final);
            float dot_prod = dot(unit_y, unit_final);
            // skew-symmetric cross-product matrix
            mat3 cp_mat = mat3::zero();
            cp_mat(0, 1) = - cross_prod[2];
            cp_mat(0, 2) = cross_prod[1];
            cp_mat(1, 0) = cross_prod[2];
            cp_mat(1, 2) = - cross_prod[0];
            cp_mat(2, 0) = - cross_prod[1];
            cp_mat(2, 1) = cross_prod[0];
            // final rotation
            mat3 rotation = mat3::identity() + cp_mat + cp_mat * cp_mat / (1 + dot_prod);
            cylinder.uniform.transform.rotation = rotation;

        }

        // translate
        cylinder.uniform.transform.translation = c.p1;

        // draw 
        draw(cylinder, scene.camera);
    }

    // disks
    for (const auto& d : body_shape.discs)
    {
        // scaling 
        disc.uniform.transform.scaling = d.radius;

        // rotation
        // Compute the rotation that maps (0,1,0) to (p2-p1)/norm(p2-p1)
        vec3 const unit_y = { 0,1,0 };
        vec3 const unit_final = (d.normal) / norm(d.normal);
        float const tolerance = 0.00001f; // 10.e^(-5)
        if (abs(dot(unit_y, unit_final) + 1) < tolerance)
        {
            // if the two unit vectors are opposite one to the other
            cylinder.uniform.transform.rotation = -mat3::identity();
        }
        else
        {
            // use cross product and dot product
            vec3 cross_prod = cross(unit_y, unit_final);
            float dot_prod = dot(unit_y, unit_final);
            // skew-symmetric cross-product matrix
            mat3 cp_mat = mat3::zero();
            cp_mat(0, 1) = -cross_prod[2];
            cp_mat(0, 2) = cross_prod[1];
            cp_mat(1, 0) = cross_prod[2];
            cp_mat(1, 2) = -cross_prod[0];
            cp_mat(2, 0) = -cross_prod[1];
            cp_mat(2, 1) = cross_prod[0];
            // final rotation
            mat3 rotation = mat3::identity() + cp_mat + cp_mat * cp_mat / (1 + dot_prod);
            cylinder.uniform.transform.rotation = rotation;

        }

        // translation
        disc.uniform.transform.translation = d.center;

        // draw
        draw(disc, scene.camera);
    }

    // Display positional constraint using spheres
    sphere.uniform.transform.scaling = 0.02f;
    sphere.uniform.color = { 1,0,0 };
    for (const auto& constraints : positional_constraints) {
        sphere.uniform.transform.translation = constraints.second;
        draw(sphere, scene.camera, shaders["mesh"]);
    }

    // Display ground
    draw(ground, scene.camera);
    glBindTexture(GL_TEXTURE_2D, scene.texture_white);
}


// Automatic detection of divergence: stop the simulation if detected
void scene_model::detect_simulation_divergence()
{
    const size_t NN = position.size();
    for (size_t k = 0; simulation_diverged == false && k < NN; ++k)
    {
        const float f = norm(force[k]);
        const vec3& p = position[k];

        if (std::isnan(f)) // detect NaN in force
        {
            std::cout << "NaN detected in forces" << std::endl;
            simulation_diverged = true;
        }

        if (f > 1000.0f) // detect strong force magnitude
        {
            std::cout << " **** Warning : Strong force magnitude detected " << f << " at vertex " << k << " ****" << std::endl;
            simulation_diverged = true;
        }

        if (std::isnan(p.x) || std::isnan(p.y) || std::isnan(p.z)) // detect NaN in position
        {
            std::cout << "NaN detected in positions" << std::endl;
            simulation_diverged = true;
        }

        if (simulation_diverged == true)
        {
            std::cerr << " **** Simulation has diverged **** " << std::endl;
            std::cerr << " > Stop simulation iterations" << std::endl;
            timer.stop();
        }
    }

}


void scene_model::set_gui()
{
    ImGui::SliderFloat("Time scale", &timer.scale, 0.05f, 2.0f, "%.2f s");
    ImGui::SliderFloat("Stiffness", &user_parameters.K, 1.0f, 400.0f, "%.2f s");
    ImGui::SliderFloat("Damping", &user_parameters.mu, 0.0f, 0.1f, "%.3f s");
    ImGui::SliderFloat("Mass", &user_parameters.m, 1.0f, 15.0f, "%.2f s");
    ImGui::SliderFloat("Wind", &user_parameters.wind, 0.0f, 50.0f, "%.2f s");
    ImGui::SliderFloat("Wind Angle", &user_parameters.wind_angle, 0.0f, 2*M_PI, "%.2f s");

    ImGui::Checkbox("Wireframe", &gui_display_wireframe);
    ImGui::Checkbox("Texture", &gui_display_texture);

    bool const stop = ImGui::Button("Stop anim"); ImGui::SameLine();
    bool const start = ImGui::Button("Start anim");

    if (stop)  timer.stop();
    if (start) {
        if (simulation_diverged)
            force_simulation = true;
        timer.start();
    }

    bool const restart = ImGui::Button("Restart");
    if (restart) initialize();
}

#endif