
#include "example_mass_spring.hpp"


#ifdef SCENE_MASS_SPRING_1D

using namespace vcl;


static void set_gui(timer_basic& timer);


/** Compute spring force applied on particle pi from particle pj */
vec3 spring_force(const vec3& pi, const vec3& pj, float L0, float K)
{
    vec3 const pji = pj - pi;
    float const L = norm(pji);
    return K * (L - L0) * pji / L;
}


void scene_model::setup_data(std::map<std::string,GLuint>& , scene_structure& , gui_structure& )
{
    // Initial position and speed of particles
    // ******************************************* //
    N = 15; // number of particles

    L0 = 1.0f / (N-1); // rest length

    for (size_t i = 0; i < N; i++)
    {
        particle_element particule;
        particule.p = {L0*i, 0, 0};    // initial position
        particule.v = {0, 0, 0};      // initial speed

        pVec.push_back(particule);
    }


    // Display elements
    // ******************************************* //
    segment_drawer.init();
    segment_drawer.uniform_parameter.color = {0,0,1};

    sphere = mesh_primitive_sphere();
    sphere.uniform.transform.scaling = 0.05f;


    std::vector<vec3> borders_segments = {{-1,-1,-1},{1,-1,-1}, {1,-1,-1},{1,1,-1}, {1,1,-1},{-1,1,-1}, {-1,1,-1},{-1,-1,-1},
                                          {-1,-1,1} ,{1,-1,1},  {1,-1,1}, {1,1,1},  {1,1,1}, {-1,1,1},  {-1,1,1}, {-1,-1,1},
                                          {-1,-1,-1},{-1,-1,1}, {1,-1,-1},{1,-1,1}, {1,1,-1},{1,1,1},   {-1,1,-1},{-1,1,1}};
    borders = borders_segments;
    borders.uniform.color = {0,0,0};

}





void scene_model::frame_draw(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& )
{
    timer.update();
    set_gui(timer);


    // Simulation time step (dt)
    float dt = timer.scale;

    // Simulation parameters
    const float m  = 0.01f/(N-1);   // particle mass
    const float K  = 5.0f;          // spring stiffness
    const float K_bend = K;       // bending spring stiffness
    const float mu = 0.005f;        // damping coefficient

    dt = 0.08f * dt * std::sqrt(m/K);

    const vec3 g   = {0,-9.81f,0}; // gravity

    // Compute forces and numerical integration for each particle except the first one
    for (size_t i = 1; i < N; i++)
    {
        // Forces
        vec3 f_spring = spring_force(pVec[i].p, pVec[i-1].p, L0, K); // previous spring
        if (i < N-1)
        {
            f_spring += spring_force(pVec[i].p, pVec[i+1].p, L0, K); // next spring
        }
        if (i > 1)
        {
            // bending with i-2
            f_spring += spring_force(pVec[i].p, pVec[i-2].p, 2*L0, K_bend);
        }
        if (i < N-2)
        {
            // bending with i+2
            f_spring += spring_force(pVec[i].p, pVec[i+2].p, 2*L0, K_bend);
        }
        if (i > 2)
        {
            // bending with i-3
            f_spring += spring_force(pVec[i].p, pVec[i-3].p, 3*L0, K_bend);
        }
        if (i < N-3)
        {
            // bending with i+3
            f_spring += spring_force(pVec[i].p, pVec[i+3].p, 3*L0, K_bend);
        }
        const vec3 f_weight =  m * g;
        const vec3 f_damping = - mu * pVec[i].v;
        const vec3 F = f_spring + f_weight + f_damping;

        // Numerical integration
        vec3& p = pVec[i].p; // position of particle
        vec3& v = pVec[i].v; // speed of particle

        /* Semi-implicit Euler */ 
        v = v + dt * F / m;
        p = p + dt * v;
    }

    // Display of the result

    // particle 0
    sphere.uniform.transform.scaling = 0.025f;
    sphere.uniform.transform.translation = pVec[0].p;
    sphere.uniform.color = {0,0,0};
    draw(sphere, scene.camera, shaders["mesh"]);

    // every other particle and springs
    for (size_t i = 1; i < N; i++)
    {
        // particle
        sphere.uniform.transform.scaling = 0.025f;
        sphere.uniform.transform.translation = pVec[i].p;
        sphere.uniform.color = {1,0,0};
        draw(sphere, scene.camera, shaders["mesh"]);

        // Spring pVec[i-1] -- pVec[i]
        segment_drawer.uniform_parameter.p1 = pVec[i-1].p;
        segment_drawer.uniform_parameter.p2 = pVec[i].p;
        segment_drawer.draw(shaders["segment_im"],scene.camera);

    }

    draw(borders, scene.camera, shaders["curve"]);
}


/** Part specific GUI drawing */
static void set_gui(timer_basic& timer)
{
    // Can set the speed of the animation
    float scale_min = 0.05f;
    float scale_max = 2.0f;
    ImGui::SliderScalar("Time scale", ImGuiDataType_Float, &timer.scale, &scale_min, &scale_max, "%.2f s");

    // Start and stop animation
    if (ImGui::Button("Stop"))
        timer.stop();
    if (ImGui::Button("Start"))
        timer.start();

}



#endif
