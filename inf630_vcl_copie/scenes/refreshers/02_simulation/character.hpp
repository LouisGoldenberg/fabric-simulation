#pragma once

#include "main/scene_base/base.hpp"

#ifdef SCENE_CHARACTER

/*
DESCRIPTION OF THE GOAL OF THIS SCENE
    Design a character (with spheres and cylinders) and to fit some cloth on it
    Ideas : super-hero like character with a cape w/ or w/out wind, girl with a skirt or dress
*/

struct user_parameters_structure
{
    float m;    // Global mass (to be divided by the number of particles)
    float K;    // Global stiffness (to be divided by the number of particles)
    float mu;   // Damping
    float wind; // Wind magnitude;
    float wind_angle; // Wind angle
};

struct simulation_parameters_structure
{
    float m;  // mass
    float L0; // spring rest length
};

struct sphere_structure
{
    vcl::vec3 center;
    float radius;
};

struct cylinder_structure
{
    vcl::vec3 p1;
    vcl::vec3 p2;
    float radius;
};

struct disc_structure
{
    vcl::vec3 center;
    float radius;
    vcl::vec3 normal;
};

// Body
struct body_shapes_structure
{
    // head 
    vcl::vec3 head_p;
    float head_r;

    // chest
    vcl::vec3 chest_top;
    vcl::vec3 chest_bottom;
    float chest_r;

    // arms
    vcl::vec3 shoulder_left;
    vcl::vec3 shoulder_right;
    vcl::vec3 elbow_left;
    vcl::vec3 elbow_right;
    vcl::vec3 hand_left;
    vcl::vec3 hand_right;
    float shoulder_r;
    float arm_r;
    float hand_r;

    // legs
    vcl::vec3 hip_left;
    vcl::vec3 knee_left;
    vcl::vec3 foot_left;
    vcl::vec3 hip_right;
    vcl::vec3 knee_right;
    vcl::vec3 foot_right;
    float leg_r;
    float foot_r;

    // shapes
    std::vector<sphere_structure> spheres;
    std::vector<cylinder_structure> cylinders;
    std::vector<disc_structure> discs;
};

// Sphere and ground used for collision
struct collision_shapes_structure
{
    float ground_height; // height of the ground (in y-coordinate)
};



struct scene_model : scene_base
{
    // Particles parameters
    vcl::buffer2D<vcl::vec3> position;
    vcl::buffer2D<vcl::vec3> speed;
    vcl::buffer2D<vcl::vec3> force;

    // Simulation parameters
    simulation_parameters_structure simulation_parameters; // parameters that user can control directly
    user_parameters_structure user_parameters;             // parameters adjusted with respect to mesh size (not controled directly by the user)

    // Cloth mesh elements
    vcl::mesh_drawable cloth;              // Visual model for the cloth
    vcl::buffer<vcl::vec3> normals;        // Normal of the cloth used for rendering and wind force computation
    vcl::buffer<vcl::uint3> connectivity;  // Connectivity of the triangular model

    // Body shapes
    body_shapes_structure body_shape;

    // Parameters of the shape used for collision
    collision_shapes_structure collision_shapes;

    // Store index and position of vertices constrained to have a fixed 3D position
    std::map<int, vcl::vec3> positional_constraints;

    // Textures
    GLuint texture_cloth;
    GLuint texture_wood;

    // Visual elements of the scene
    vcl::mesh_drawable sphere;
    vcl::mesh_drawable cylinder;
    vcl::mesh_drawable disc;
    vcl::mesh_drawable ground;

    // Gui parameters
    bool gui_display_wireframe;
    bool gui_display_texture;

    // Parameters used to control if the simulation runs when a numerical divergence is detected
    bool simulation_diverged; // Active when divergence is detected
    bool force_simulation;    // Force to run simulation even if divergence is detected
    GLuint shader_mesh;

    vcl::timer_event timer;



    void initialize();
    void collision_constraints();
    void compute_forces();
    void numerical_integration(float h);
    void detect_simulation_divergence();
    void hard_constraints();
    void set_gui();


    void setup_data(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure& gui);
    void frame_draw(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure& gui);
    void display_elements(std::map<std::string, GLuint>& shaders, scene_structure& scene, gui_structure& gui);
};

#endif