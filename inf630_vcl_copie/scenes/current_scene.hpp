#pragma once

// Indicate here which exercise should be currently considered using its define keyword
#define SCENE_CHARACTER // replace this keyword to change the scene

// Help: The keywords associated to other scenes are listed below
//
// #define SCENE_DEFAULT_3D_GRAPHICS
// #define SCENE_INTERPOLATION_POSITION
// #define SCENE_ARTICULATED_HIERARCHY
// #define SCENE_MASS_SPRING_1D
// #define SCENE_CLOTH
// #define SCENE_CURTAINS
// #define SCENE_CURTAINS_HACKED
// #define SCENE_CHARACTER
// #define SCENE_SKINNING