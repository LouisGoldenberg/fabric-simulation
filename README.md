# Computer Graphics Project: Fabric Simulation
Valnet Milla & Goldenberg Louis

### Comments
* The code basis is taken from Damien Rohmer's courses on computer graphics, and adapted from there.
* Our idea was to model visually-realistic fabric behavior, and to be able to make nice representations.
* Essential part of our work is in: inf630_vcl_copie/scenes/refreshers/02_simulation/ (\*.cpp and \*.hpp files)
* Main branch contains anything we have made functional, theater_curtains-develop is the actual state of our intersection computation

### Building instructions
* Use Cmake (Windows and Linux)
* WARNING : we did not use the standard Makefile, so it is not actual anymore and will probably not work :) 

### Others
* Whatever you find necessary
